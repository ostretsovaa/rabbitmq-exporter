package main

import (
	"encoding/json"
	"fmt"
	"github.com/ostretsov/x"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"
)

type queue struct {
	Name     string `json:"name"`
	Messages int    `json:"messages"`
}

var qsLock sync.RWMutex
var queueSizes = make(map[string]int)

func main() {
	srvAddr := x.MustGetenv("SERVER_ADDRESS")
	urlEnv := x.MustGetenv("RABBITMQ_URL")
	authTokenEnv := x.MustGetenv("RABBITMQ_AUTH_TOKEN")
	queuesEnv := x.MustGetenv("RABBITMQ_QUEUES")
	queues := strings.Split(queuesEnv, ",")

	go updateQueueSizes(urlEnv, authTokenEnv, queues)

	err := http.ListenAndServe(srvAddr, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		qsLock.RLock()
		defer qsLock.RUnlock()

		w.WriteHeader(http.StatusOK)
		for qn, qsz := range queueSizes {
			_, err := fmt.Fprintf(w, "queue.%s %d\n", qn, qsz)
			if err != nil {
				log.Println("failed to write response", err)
				return
			}
		}
	}))
	log.Println("server stopped", err)
}

func updateQueueSizes(urlEnv, authTokenEnv string, queues []string) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("panicked", r)
		}
		go updateQueueSizes(urlEnv, authTokenEnv, queues)
	}()

	for {
		qs, err := getQueues(urlEnv, authTokenEnv)
		if err != nil {
			log.Println("failed to get queues data", err)
			time.Sleep(1 * time.Second)
			continue
		}
		qsFiltered := x.Filter(qs, func(q queue) bool {
			return x.Contains(queues, q.Name)
		})

		qsLock.Lock()
		for _, q := range qsFiltered {
			queueSizes[q.Name] = q.Messages
		}
		qsLock.Unlock()

		time.Sleep(1 * time.Minute)
	}
}

func getQueues(url, authToken string) ([]queue, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Basic "+authToken)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("expected 200; got %d status code", resp.StatusCode)
	}

	var qs []queue
	err = json.NewDecoder(resp.Body).Decode(&qs)
	return qs, err
}
