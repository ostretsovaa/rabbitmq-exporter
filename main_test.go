package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_getQueues(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(rabbitMQResp))
	}))

	qs, err := getQueues(srv.URL, "token")
	if err != nil {
		t.Fatal("failed getQueues", err)
	}
	if len(qs) != 2 {
		t.Error("want 2; got", len(qs))
	}
}

var rabbitMQResp = `[
  {
    "arguments": {},
    "auto_delete": false,
    "backing_queue_status": {
      "avg_ack_egress_rate": 0,
      "avg_ack_ingress_rate": 0,
      "avg_egress_rate": 0,
      "avg_ingress_rate": 0,
      "delta": [
        "delta",
        "undefined",
        0,
        0,
        "undefined"
      ],
      "len": 181,
      "mode": "default",
      "next_deliver_seq_id": 0,
      "next_seq_id": 181,
      "q1": 0,
      "q2": 0,
      "q3": 0,
      "q4": 181,
      "target_ram_count": "infinity",
      "version": 1
    },
    "consumer_capacity": 0,
    "consumer_utilisation": 0,
    "consumers": 0,
    "durable": true,
    "effective_policy_definition": {},
    "exclusive": false,
    "exclusive_consumer_tag": null,
    "garbage_collection": {
      "fullsweep_after": 65535,
      "max_heap_size": 0,
      "min_bin_vheap_size": 46422,
      "min_heap_size": 233,
      "minor_gcs": 332
    },
    "head_message_timestamp": 1657651447,
    "idle_since": "2022-10-11T14:22:02.845+00:00",
    "memory": 200128,
    "message_bytes": 9774,
    "message_bytes_paged_out": 0,
    "message_bytes_persistent": 9774,
    "message_bytes_ram": 9774,
    "message_bytes_ready": 9774,
    "message_bytes_unacknowledged": 0,
    "message_stats": {
      "publish": 177,
      "publish_details": {
        "rate": 0
      }
    },
    "messages": 181,
    "messages_details": {
      "rate": 0
    },
    "messages_paged_out": 0,
    "messages_persistent": 181,
    "messages_ram": 181,
    "messages_ready": 181,
    "messages_ready_details": {
      "rate": 0
    },
    "messages_ready_ram": 181,
    "messages_unacknowledged": 0,
    "messages_unacknowledged_details": {
      "rate": 0
    },
    "messages_unacknowledged_ram": 0,
    "name": "_internal.manifest_close.notify_packages",
    "node": "rabbit@rabbitmq",
    "operator_policy": null,
    "policy": null,
    "recoverable_slaves": null,
    "reductions": 301037331,
    "reductions_details": {
      "rate": 0
    },
    "single_active_consumer_tag": null,
    "state": "running",
    "type": "classic",
    "vhost": "/"
  },
  {
    "arguments": {},
    "auto_delete": false,
    "backing_queue_status": {
      "avg_ack_egress_rate": 0,
      "avg_ack_ingress_rate": 0,
      "avg_egress_rate": 0,
      "avg_ingress_rate": 0,
      "delta": [
        "delta",
        "undefined",
        0,
        0,
        "undefined"
      ],
      "len": 1,
      "mode": "default",
      "next_deliver_seq_id": 1,
      "next_seq_id": 1,
      "q1": 0,
      "q2": 0,
      "q3": 0,
      "q4": 1,
      "target_ram_count": "infinity",
      "version": 1
    },
    "consumer_capacity": 0,
    "consumer_utilisation": 0,
    "consumers": 0,
    "durable": true,
    "effective_policy_definition": {},
    "exclusive": false,
    "exclusive_consumer_tag": null,
    "garbage_collection": {
      "fullsweep_after": 65535,
      "max_heap_size": 0,
      "min_bin_vheap_size": 46422,
      "min_heap_size": 233,
      "minor_gcs": 63564
    },
    "head_message_timestamp": 1657871058,
    "idle_since": "2022-10-11T14:22:02.751+00:00",
    "memory": 21520,
    "message_bytes": 367,
    "message_bytes_paged_out": 0,
    "message_bytes_persistent": 367,
    "message_bytes_ram": 367,
    "message_bytes_ready": 367,
    "message_bytes_unacknowledged": 0,
    "message_stats": {
      "ack": 0,
      "ack_details": {
        "rate": 0
      },
      "deliver": 0,
      "deliver_details": {
        "rate": 0
      },
      "deliver_get": 2,
      "deliver_get_details": {
        "rate": 0
      },
      "deliver_no_ack": 0,
      "deliver_no_ack_details": {
        "rate": 0
      },
      "get": 2,
      "get_details": {
        "rate": 0
      },
      "get_empty": 0,
      "get_empty_details": {
        "rate": 0
      },
      "get_no_ack": 0,
      "get_no_ack_details": {
        "rate": 0
      },
      "publish": 1,
      "publish_details": {
        "rate": 0
      },
      "redeliver": 1,
      "redeliver_details": {
        "rate": 0
      }
    },
    "messages": 1,
    "messages_details": {
      "rate": 0
    },
    "messages_paged_out": 0,
    "messages_persistent": 1,
    "messages_ram": 1,
    "messages_ready": 1,
    "messages_ready_details": {
      "rate": 0
    },
    "messages_ready_ram": 1,
    "messages_unacknowledged": 0,
    "messages_unacknowledged_details": {
      "rate": 0
    },
    "messages_unacknowledged_ram": 0,
    "name": "_internal.service.consolidation",
    "node": "rabbit@rabbitmq",
    "operator_policy": null,
    "policy": null,
    "recoverable_slaves": null,
    "reductions": 94402636,
    "reductions_details": {
      "rate": 0
    },
    "single_active_consumer_tag": null,
    "state": "running",
    "type": "classic",
    "vhost": "/"
  }
]`
