FROM golang:1.18 as builder
RUN adduser \
    --disabled-password \
    --gecos "" \
    --no-create-home \
    appuser
WORKDIR /go/src/app
COPY . .
RUN go mod download

RUN CGO_ENABLED=0 go build -o /go/bin/app .

FROM scratch
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/app /app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
USER appuser:appuser
ENTRYPOINT ["/app"]
